ALL:
	sh ./builddir.sh
	javac -d ./build src/bolDemo/main/jeroenmathon/*.java \
	 src/bolDemo/database/jeroenmathon/*.java \
	 src/bolDemo/gui/jeroenmathon/*.java

run:
	java -cp ./build/ bolDemo.main.jeroenmathon.main