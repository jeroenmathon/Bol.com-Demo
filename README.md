# Bol.com-Demo
A demo for Bol.com containing my Programming practices.
This program is a going to be a Police Database(Of course not official)
It will have a sqlite database allowing you to look at "criminals"
Add and remove records using a graphical user interface.

# Documentation
The documentation for this project can be found in the source code and [here](http://jeroenmathon.github.io/Bol.com-Demo/Documentation/)
# License?
This entire project is opensource and you are free to use it for learning and other purposes.
