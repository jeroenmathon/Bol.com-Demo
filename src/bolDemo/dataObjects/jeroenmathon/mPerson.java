package bolDemo.dataObjects.jeroenmathon;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by jeroen on 6-3-16.
 */

/**
 * An mObject for Persons
 */
public class mPerson extends mObject {
    private Image personPicture;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private final String picturePath;

    mPerson(
            int id, String firstName, String lastName,
            String dateOfBirth, String picturePath) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.picturePath = picturePath;
    }

    /**
     * Returns the persons first name
     * @return String: The persons first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returns the persons last name
     * @return String: The persons last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Returns the persons date of birth
     * @return String: The persons date of birth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Returns the persons picture
     * @return BufferedImage: The persons picture
     */
    public Image getPersonPicture() {
        return personPicture;
    }

    /**
     * Returns the path of the persons picture
     * @return String: The path of the persons picture
     */
    public String getPicturePath() {
        return picturePath;
    }

    /**
     * Sets the persons first name
     * @param firstName: The persons first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Sets the persons last name
     * @param lastName: The persons last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Sets the persons date of birth
     * @param dateOfBirth: The persons date of birth
     */
    public void setDateOfBirth(String dateOfBirth) {
       this.dateOfBirth = dateOfBirth;
    }

    /**
     * Sets the persons picture
     * @param picturePath: The path to the persons picture
     */
    public void setPersonPicture(String picturePath) {
        try {
            personPicture = ImageIO.read(new File(picturePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
