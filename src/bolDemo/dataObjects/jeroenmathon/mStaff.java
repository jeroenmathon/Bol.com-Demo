package bolDemo.dataObjects.jeroenmathon;

/**
 * Created by jeroen on 6-3-16.
 */

/**
 * An mPersons object for Staff members
 */
public class mStaff extends mPerson {
    private String phone, email, location, rank;

    public mStaff(
            int id, String firstName, String lastName, String dateOfBirth, String picturePath,
            String phone, String email, String location, String rank) {
        super(id, firstName, lastName, dateOfBirth, picturePath);

        this.phone = phone;
        this.email = email;
        this.location = location;
        this.rank = rank;
    }


    /**
     * Returns the staff members phone number
     * @return String: The staff members phone number
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Returns the staff members email address
     * @return String: The staff members email address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Returns the staff members address
     * @return String: The staff members address
     */
    public String getLocation() {
        return location;
    }

    /**
     * Returns the staff members rank
     * @return String: The staff members rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * Sets the staff members phone number
     * @param phone: The staff members phone number
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Sets the staff members email address
     * @param email: The staff members email address
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Sets the staff members location
     * @param location: The staff members location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Sets the staff members rank
     * @param rank: The staff members rank
     */
    public void setRank(String rank) {
        this.rank = rank;
    }
}
