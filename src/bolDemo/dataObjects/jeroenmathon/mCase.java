package bolDemo.dataObjects.jeroenmathon;

/**
 * Created by jeroen on 6-3-16.
 */

/**
 * An mObject for cases
 */
public class mCase extends mObject{

    private String name, description, startDate, caseStarter, caseMaintainer;

    public mCase(
            int id, String name, String description, String startDate,
            String caseStarter, String caseMaintainer) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.caseStarter = caseStarter;
        this.caseMaintainer = caseMaintainer;
    }

    /**
     * Returns the name of the case
     * @return String: The name of the case
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the description of the case
     * @return String: The description of the case
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the start date of the case
     * @return String: The start date of the case
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Returns the staff member that opened the case
     * @return String: The staff member that opened the case
     */
    public String getCaseStarter() {
        return caseStarter;
    }

    /**
     * Returns the staff member that maintains the case
     * @return String: The staff member that maintains the case
     */
    public String getCaseMaintainer() {
        return caseMaintainer;
    }

    /**
     * Sets the name of the case
     * @param name: The name of the case
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the description of the case
     * @param description: The description of the case
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the start date of the case
     * @param startDate: The start date of the case
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * Sets the staff member that opened the case
     * @param caseStarter: The staff member that opened the case
     */
    public void setCaseStarter(String caseStarter) {
        this.caseStarter = caseStarter;
    }

    /**
     * Sets the staff member that maintains the case
     * @param caseMaintainer: The staff member that maintains the case
     */
    public void setCaseMaintainer(String caseMaintainer) {
        this.caseMaintainer = caseMaintainer;
    }
}
