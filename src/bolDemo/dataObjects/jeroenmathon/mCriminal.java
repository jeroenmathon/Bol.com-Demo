package bolDemo.dataObjects.jeroenmathon;

/**
 * Created by jeroen on 6-3-16.
 */

/**
 * An mPerson object for criminals
 */
public class mCriminal extends mPerson {
    private final String offense;
    private final String sentenceTime;
    private final String status;

    public mCriminal(
            int id, String firstName, String lastName, String dateOfBirth,
            String picturePath,String sentenceTime, String offense, String status) {
        super(id, firstName, lastName, dateOfBirth, picturePath);

        this.offense = offense;
        this.sentenceTime = sentenceTime;
        this.status = status;
    }


    /**
     * Returns the criminals offense
     * @return String: The criminals offense
     */
    public String getOffense() {
        return offense;
    }

    /**
     * Returns the criminals sentence time
     * @return String: The criminals sentence time
     */
    public String getSentenceTime() {
        return sentenceTime;
    }

    /**
     * Returns the criminals status
     * @return String: The criminals status
     */
    public String getStatus() {
        return status;
    }
}
