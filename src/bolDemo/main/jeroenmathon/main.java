package bolDemo.main.jeroenmathon;

import bolDemo.database.jeroenmathon.dbMgr;
import bolDemo.gui.jeroenmathon.guiClass;
/**
 * Created by jeroen on 3-3-16.
 */

/**
 * Class: main
 * Description: The start point of the programs execution
 */
class main {
    public static void main(String[] args) {
        //Construct needed objects
        guiClass gClass = new guiClass();
    }
}
