package bolDemo.gui.jeroenmathon;

import bolDemo.dataObjects.jeroenmathon.mCase;
import bolDemo.dataObjects.jeroenmathon.mCriminal;
import bolDemo.dataObjects.jeroenmathon.mStaff;
import bolDemo.database.jeroenmathon.dbMgr;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by jeroen on 7-3-16.
 */

/**
 * This class handles the browser panel interfaces
 */
class browsePanel implements ActionListener{
    final JPanel browsePanel = new JPanel();
    final JTable tbl = new JTable() {
        private static final long serialVersionUID = 1L;

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    final DefaultTableModel dtm = new DefaultTableModel(0, 0);
    String[] header;
    final ArrayList<Object> rowObjects = new ArrayList<>();
    int rLimit;
    private final JPanel actionButtonPanel = new JPanel();

    private final JButton mAdd = new JButton("Add");
    private final JButton mView = new JButton("View");
    private final JButton mDelete = new JButton("Delete");
    private JTabbedPane tabbedPane;
    final dbMgr db;

    browsePanel(dbMgr db) {
        this.db = db;
        browsePanel.setLayout(new BoxLayout(browsePanel, BoxLayout.Y_AXIS));
        mAdd.addActionListener(this);
        mView.addActionListener(this);
        mDelete.addActionListener(this);
    }

    /**
     * Initializes the main table
     */
    void initialize() {

        // Add header to table model
        dtm.setColumnIdentifiers(header);

        //Set table mode;
        tbl.setModel(dtm);

        // Add rows into table
        for (int i = 0; i < rowObjects.size(); i++) {
            dtm.addRow((Object[]) rowObjects.get(i));
        }

        JScrollPane browseTable = new JScrollPane(tbl);
        browsePanel.add(browseTable);
        actionButtonPanel.add(mAdd);
        actionButtonPanel.add(mView);
        actionButtonPanel.add(mDelete);
        browsePanel.add(actionButtonPanel);
    }

    /**
     * Sets the tabbed pane
     * @param tabbedPane: The tabbed pane to set
     */
    public void setTab(JTabbedPane tabbedPane) {
        this.tabbedPane = tabbedPane;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(actionEvent.getSource() == mAdd) {
            tabbedPane.setSelectedIndex(0);
        } else if(actionEvent.getSource() == mView) {
            tabbedPane.setSelectedIndex(0);
            getRowData();

        } else if(actionEvent.getSource() == mDelete) {
            removeRecord();
        }
    }

    /**
     * Retrieves the data stored in the currently selected row
     */
    void getRowData() {

    }

    /**
     * Removes a record from the database
     */
    void removeRecord() {
    }
}
//@TODO Try to clean the code up by using class inheritance for staff, criminal and case browse panel
class staffBrowsePanel extends browsePanel {

    private final staffInfo staffPanel;
    public staffBrowsePanel(dbMgr db, staffInfo staffPanel) {
        super(db);
        header = new String[] {
                "Picture", "ID", "First Name", "Last Name", "Date of Birth", "Phone", "Email", "Location", "Rank"};
        this.staffPanel = staffPanel;
    }

    /**
     * Builds upon super.initialize to mAdd specialized functionality to the tables
     */
    public void initialize() {
        ArrayList<Integer> ids = db.getIds("staff");
        ArrayList<mStaff> staffMembs = new ArrayList<>();

        for (int i = 0; i < ids.size(); i++) {
            staffMembs.add(db.getStaff(ids.get(i)));
        }
        for (int i = 0; i < staffMembs.size(); i++) {
            rowObjects.add(new Object[] {
                    "NULL", staffMembs.get(i).getId(), staffMembs.get(i).getFirstName(),
                    staffMembs.get(i).getLastName(), staffMembs.get(i).getDateOfBirth(), staffMembs.get(i).getPhone(),
                    staffMembs.get(i).getEmail(), staffMembs.get(i).getLocation(),
                    staffMembs.get(i).getRank()});
        }

        rLimit = db.rowCount("staff");
        super.initialize();
    }

    //@TODO Apply polymorphism to reduce code and increase efficiency
    protected void getRowData() {
        staffPanel.setEditable(false);
        staffPanel.mSave.setEnabled(false);
        staffPanel.id.setText(tbl.getValueAt(tbl.getSelectedRow(), 1).toString());
        staffPanel.firstName.setText(tbl.getValueAt(tbl.getSelectedRow(), 2).toString());
        staffPanel.lastName.setText(tbl.getValueAt(tbl.getSelectedRow(),3).toString());
        staffPanel.dateOfBirth.setText(tbl.getValueAt(tbl.getSelectedRow(), 4).toString());
        staffPanel.phone.setText(tbl.getValueAt(tbl.getSelectedRow(), 5).toString());
        staffPanel.email.setText(tbl.getValueAt(tbl.getSelectedRow(), 6).toString());
        staffPanel.location.setText(tbl.getValueAt(tbl.getSelectedRow(), 7).toString());
        staffPanel.rank.setText(tbl.getValueAt(tbl.getSelectedRow(), 8).toString());
    }

    protected void removeRecord() {
        db.removeRecord(Integer.parseInt(tbl.getValueAt(tbl.getSelectedRow(), 1).toString()), "staff");
        dtm.removeRow(tbl.getSelectedRow());
    }
}

class criminalBrowsePanel extends browsePanel {
    private final criminalInfo criminalPanel;

    public criminalBrowsePanel(dbMgr db, criminalInfo criminalPanel) {
        super(db);
        header = new String[] {
                "Picture", "ID", "First Name", "Last Name", "Date of Birth", "Offense", "Sentence Length", "Status"};
        this.criminalPanel = criminalPanel;
    }

    /**
     * Builds upon super.initialize to mAdd specialized functionality to the tables
     */
    public void initialize() {
        ArrayList<Integer> ids = db.getIds("criminals");
        ArrayList<mCriminal> criminals = new ArrayList<>();

        for (int i = 0; i < ids.size(); i++) {
            criminals.add(db.getCriminal(ids.get(i)));
        }
        for (int i = 0; i < criminals.size(); i++) {
            rowObjects.add(new Object[] {
                    "NULL", criminals.get(i).getId(), criminals.get(i).getFirstName(), criminals.get(i).getLastName(),
                    criminals.get(i).getDateOfBirth(), criminals.get(i).getOffense(), criminals.get(i).getSentenceTime(),
                    criminals.get(i).getStatus()});
        }

        rLimit = db.rowCount("criminals");
        super.initialize();
    }

    protected void getRowData() {
        criminalPanel.setEditable(false);
        criminalPanel.mSave.setEnabled(false);
        criminalPanel.id.setText(tbl.getValueAt(tbl.getSelectedRow(), 1).toString());
        criminalPanel.firstName.setText(tbl.getValueAt(tbl.getSelectedRow(), 2).toString());
        criminalPanel.lastName.setText(tbl.getValueAt(tbl.getSelectedRow(), 3).toString());
        criminalPanel.dateOfBirth.setText(tbl.getValueAt(tbl.getSelectedRow(), 4).toString());
        criminalPanel.offense.setText(tbl.getValueAt(tbl.getSelectedRow(), 5).toString());
        criminalPanel.sentenceLength.setText(tbl.getValueAt(tbl.getSelectedRow(), 6).toString());
        criminalPanel.status.setText(tbl.getValueAt(tbl.getSelectedRow(), 7).toString());
    }

    protected void removeRecord() {
        db.removeRecord(Integer.parseInt(tbl.getValueAt(tbl.getSelectedRow(), 1).toString()), "criminals");
        dtm.removeRow(tbl.getSelectedRow());
    }
}

class caseBrowsePanel extends browsePanel {
    private final caseInfo casePanel;
    public caseBrowsePanel(dbMgr db, caseInfo casePanel) {
        super(db);
        header = new String[] {"ID", "Name", "Description", "Start date", "Case Opener", "Case Maintainer"};
        this.casePanel = casePanel;
    }

    /**
     * Builds upon super.initialize to mAdd specialized functionality to the tables
     */
    public void initialize() {
        ArrayList<Integer> ids = db.getIds("cases");
        ArrayList<mCase> cases = new ArrayList<>();

        for (int i = 0; i < ids.size(); i++) {
            cases.add(db.getCase(ids.get(i)));
        }
        for (int i = 0; i < cases.size(); i++) {
            rowObjects.add(new Object[] {
                    cases.get(i).getId(), cases.get(i).getName(), cases.get(i).getDescription(),
                    cases.get(i).getStartDate(), cases.get(i).getCaseStarter(), cases.get(i).getCaseMaintainer()});
        }

        rLimit = db.rowCount("cases");
        super.initialize();
    }

    protected void getRowData() {
        casePanel.setEditable(false);
        casePanel.mSave.setEnabled(false);
        casePanel.id.setText(tbl.getValueAt(tbl.getSelectedRow(), 0).toString());
        casePanel.name.setText(tbl.getValueAt(tbl.getSelectedRow(), 1).toString());
        casePanel.description.setText(tbl.getValueAt(tbl.getSelectedRow(), 2).toString());
        casePanel.startDate.setText(tbl.getValueAt(tbl.getSelectedRow(), 3).toString());
        casePanel.caseStarter.setText(tbl.getValueAt(tbl.getSelectedRow(), 4).toString());
        casePanel.caseMaintainer.setText(tbl.getValueAt(tbl.getSelectedRow(), 5).toString());
    }

    protected void removeRecord() {
        db.removeRecord(Integer.parseInt(tbl.getValueAt(tbl.getSelectedRow(), 0).toString()), "cases");
        dtm.removeRow(tbl.getSelectedRow());
    }
}