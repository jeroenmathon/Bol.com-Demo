package bolDemo.gui.jeroenmathon;

import bolDemo.database.jeroenmathon.dbMgr;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Created by jeroen on 3-3-16.
 */

/**
 * Handles the swing GUI
 */
public class guiClass {
    private dbMgr db = new dbMgr();

    public guiClass() {
        JFrame f = new mainFrame();
        f.setVisible(true);
    }


    /**
     * Constructs the main interface of the gui
     */
    class mainFrame extends JFrame {
        // Menubar creation
        final JMenuBar menuBar = new JMenuBar();

        // mainMenu
        final JMenu mainMenu;
        final JMenuItem mConnect;
        final JMenuItem mDisconnect;

        // Main tab pane
        final JTabbedPane mainTabs = new JTabbedPane();
        final JTabbedPane staffTab = new JTabbedPane();
        final JTabbedPane criminalsTab = new JTabbedPane();
        final JTabbedPane casesTab = new JTabbedPane();

        // Staff panel
        final staffInfo staffPanel = new staffInfo(db);
        final staffBrowsePanel staffBrowsePanel = new staffBrowsePanel(db, staffPanel);

        // Criminals panel
        final criminalInfo criminalPanel = new criminalInfo(db);
        final criminalBrowsePanel criminalBrowse = new criminalBrowsePanel(db, criminalPanel);

        // Cases panel
        final caseInfo casePanel = new caseInfo(db);
        final caseBrowsePanel caseBrowse = new caseBrowsePanel(db, casePanel);

        // Connect dialog
        final JFrame connectFrame = new JFrame();

        public mainFrame() {
            // Main window creation
            setTitle("Bol.com Demo");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(1400, 800);
            setLocationRelativeTo(null);

            setResizable(false);

            // Disable interface
            disableInterface();

            // mainMenu
            mainMenu = new JMenu("main");

            mainMenu.setMnemonic(KeyEvent.VK_A);
            mainMenu.getAccessibleContext().setAccessibleDescription(
                    "The main menu that contains the most important features");
            menuBar.add(mainMenu);

            // mainMenu new file item
            mConnect = new JMenuItem("Connect", KeyEvent.VK_N);
            mConnect.setAccelerator(KeyStroke.getKeyStroke(
                    KeyEvent.VK_C, ActionEvent.CTRL_MASK
            ));
            mConnect.getAccessibleContext().setAccessibleDescription(
                    "Connect to a MySQL database"
            );
            mConnect.setActionCommand("connect");
            mConnect.addActionListener(this::actionPerformed);
            mainMenu.add(mConnect);

            // mainMenu open item
            mDisconnect = new JMenuItem("Disconnect", KeyEvent.VK_D);
            mDisconnect.setAccelerator(KeyStroke.getKeyStroke(
                    KeyEvent.VK_D, ActionEvent.CTRL_MASK
            ));
            mDisconnect.getAccessibleContext().setAccessibleDescription(
                    "Disconnect from the MySQL Database"
            );
            mDisconnect.setActionCommand("disconnect");
            mDisconnect.addActionListener(this::actionPerformed);
            mainMenu.add(mDisconnect);

            menuBar.add(mainMenu);
            setJMenuBar(menuBar);

            // Main tabs panel
            mainTabs.setEnabled(false);
            this.add(mainTabs);

            // Initialize content panels
            initStaffPanel();
            initCriminalsPanel();
            initCasesPanel();
        }

        private void openConnectFrame() {
            JPanel connectDataPanel = new JPanel();
            JPanel connectButtonPanel = new JPanel();

            // Set layout managers
            connectButtonPanel.setLayout(new BoxLayout(connectButtonPanel, BoxLayout.X_AXIS));
            connectDataPanel.setLayout(new BoxLayout(connectDataPanel, BoxLayout.Y_AXIS));

            // Data fields
            JTextArea serverAddress = new JTextArea();
            JTextArea serverPort = new JTextArea();
            JTextArea serverSchema = new JTextArea();
            JTextArea serverUser = new JTextArea();
            JPasswordField serverPassword = new JPasswordField();
            serverAddress.setLineWrap(true);
            serverAddress.setPreferredSize(new Dimension(100, 100));
            // Data frames
            JPanel serverAddressInputFrame = new JPanel(new GridLayout(0, 1));
            JPanel serverPortInputFrame = new JPanel(new GridLayout(0, 1));
            JPanel serverSchemaInputFrame = new JPanel(new GridLayout(0, 1));
            JPanel serverUserInputFrame = new JPanel(new GridLayout(0, 1));
            JPanel serverPasswordInputFrame = new JPanel(new GridLayout(0, 1));

            // Titles for frames
            TitledBorder serverAddressTitle = BorderFactory.createTitledBorder("Server Address");
            TitledBorder serverPortTitle = BorderFactory.createTitledBorder("Server Port");
            TitledBorder serverSchemaTitle = BorderFactory.createTitledBorder("Database Schema");
            TitledBorder serverUserTitle = BorderFactory.createTitledBorder("Username");
            TitledBorder serverPasswordTitle = BorderFactory.createTitledBorder("Password");

            // Configure Titles Borders
            serverAddressTitle.setTitleJustification(TitledBorder.CENTER);
            serverPortTitle.setTitleJustification(TitledBorder.CENTER);
            serverSchemaTitle.setTitleJustification(TitledBorder.CENTER);
            serverUserTitle.setTitleJustification(TitledBorder.CENTER);
            serverPasswordTitle.setTitleJustification(TitledBorder.CENTER);

            // Set the borders of the frames
            serverAddressInputFrame.setBorder(serverAddressTitle);
            serverPortInputFrame.setBorder(serverPortTitle);
            serverSchemaInputFrame.setBorder(serverSchemaTitle);
            serverUserInputFrame.setBorder(serverUserTitle);
            serverPasswordInputFrame.setBorder(serverPasswordTitle);

            // Add text fields to input frames
            serverAddressInputFrame.add(serverAddress);
            serverPortInputFrame.add(serverPort);
            serverSchemaInputFrame.add(serverSchema);
            serverUserInputFrame.add(serverUser);
            serverPasswordInputFrame.add(serverPassword);

            // Add input frames to data panel
            connectDataPanel.add(serverAddressInputFrame);
            connectDataPanel.add(Box.createRigidArea(new Dimension(1, 0)));
            connectDataPanel.add(serverPortInputFrame);
            connectDataPanel.add(Box.createRigidArea(new Dimension(1, 0)));
            connectDataPanel.add(serverSchemaInputFrame);
            connectDataPanel.add(Box.createRigidArea(new Dimension(1, 0)));
            connectDataPanel.add(serverUserInputFrame);
            connectDataPanel.add(Box.createRigidArea(new Dimension(1, 0)));
            connectDataPanel.add(serverPasswordInputFrame);

            JButton mConnect = new JButton("Connect");
            JButton mCancel = new JButton("Cancel");

            connectButtonPanel.add(mCancel);
            connectButtonPanel.add(mConnect);

            connectFrame.setTitle("Connect");
            connectFrame.setSize(800, 500);
            connectFrame.setLocationRelativeTo(null);

            connectFrame.add(connectDataPanel);
            connectFrame.add(connectButtonPanel);

            connectFrame.setVisible(true);
        }

        /**
         * Initializes the staffs panel
         */
        private void initStaffPanel() {
            // Initialize tabs
            mainTabs.addTab("Staff", staffTab);
            //staffInfo.setLayout(new BoxLayout(staffInfo, BoxLayout.X_AXIS));
            staffPanel.initializeStaffInfo();
            staffTab.addTab("Info", staffPanel.infoPanel);
            staffBrowsePanel.initialize();
            staffTab.addTab("Browse", staffBrowsePanel.browsePanel);
            staffPanel.setTab(staffTab);
            staffBrowsePanel.setTab(staffTab);
        }

        /**
         * Initializes the criminals panel
         */
        private void initCriminalsPanel() {
            // Initialize tabs
            mainTabs.addTab("Criminals", criminalsTab);
            criminalPanel.initialize();
            criminalsTab.addTab("Info", criminalPanel.infoPanel);
            criminalBrowse.initialize();
            criminalsTab.addTab("Browse", criminalBrowse.browsePanel);
            criminalPanel.setTab(criminalsTab);
            criminalBrowse.setTab(criminalsTab);
        }

        /**
         * Initializes the cases panel
         */
        private void initCasesPanel() {
            // Initialize tabs
            mainTabs.addTab("Cases", casesTab);
            casePanel.initialize();
            casesTab.addTab("Info", casePanel.infoPanel);
            caseBrowse.initialize();
            casesTab.addTab("Browse", caseBrowse.browsePanel);
            casePanel.setTab(casesTab);
            caseBrowse.setTab(casesTab);
        }

        /**
         * The action handler
         */
        private void actionPerformed(ActionEvent e) {
            if ("connect".equals(e.getActionCommand())) {
                // Do connection stuff
                //openConnectFrame(); //TODO Create connect frame
                db = new dbMgr();
                enableInterface();
            } else if ("disconnect".equals(e.getActionCommand())) {
                db = null;
                disableInterface();
            }
        }

        /**
         * Enables all interface components
         */
        private void enableInterface() {
            mainTabs.setEnabled(true);
            staffTab.setEnabled(true);
            criminalsTab.setEnabled(true);
            casesTab.setEnabled(true);
            staffPanel.setEnabled(true);
            criminalPanel.setEnabled(true);
            casePanel.setEnabled(true);
        }

        /**
         * Disables all interface components
         */
        private void disableInterface() {
            mainTabs.setEnabled(false);
            staffTab.setEnabled(false);
            criminalsTab.setEnabled(false);
            casesTab.setEnabled(false);
            staffPanel.setEnabled(false);
            criminalPanel.setEnabled(false);
            casePanel.setEnabled(false);
        }
    }
}