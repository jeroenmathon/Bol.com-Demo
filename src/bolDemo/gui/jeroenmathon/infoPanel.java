package bolDemo.gui.jeroenmathon;

import bolDemo.database.jeroenmathon.dbMgr;
import org.omg.CORBA.INTERNAL;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by jeroen on 7-3-16.
 */

/**
 * Handles the information panels
 */
class infoPanel implements ActionListener {
    final JPanel infoPanel = new JPanel();
    final JLabel id = new JLabel();
    final JPanel dataFrame = new JPanel();
    private final JPanel idFrame = new JPanel(new GridLayout(0, 1));
    final JPanel buttonFrame = new JPanel();
    final JButton mSave = new JButton("Save");
    private final JButton mEdit = new JButton("Edit");
    private final JButton mAdd = new JButton("Add");
    private final JButton mDelete = new JButton("Delete");
    dbMgr db = null;
    private boolean addBool = false;

    infoPanel(dbMgr db) {
        this.db = db;
        mSave.addActionListener(this);
        mEdit.addActionListener(this);
        mAdd.addActionListener(this);
        mDelete.addActionListener(this);
    }

    /**
     * Initializes core panel components
     */
    void initialize() {
        // Initialize tab
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.X_AXIS));
        dataFrame.setLayout(new GridLayout(0, 1));

        TitledBorder dataTitle = BorderFactory.createTitledBorder("Data");
        dataTitle.setTitleJustification(TitledBorder.CENTER);
        dataFrame.setBorder(dataTitle);
        dataFrame.setPreferredSize(new Dimension(1100, 450));

        id.setMaximumSize(new Dimension(10, 15));

        // Container for the ID
        TitledBorder idTitle = BorderFactory.createTitledBorder("ID");
        idFrame.setBorder(idTitle);
        idFrame.add(id);

        dataFrame.add(idFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));

        mAdd.addActionListener(this);
        mEdit.addActionListener(this);
        mSave.addActionListener(this);

        buttonFrame.setLayout(new BoxLayout(buttonFrame, BoxLayout.LINE_AXIS));
        buttonFrame.add(mAdd);
        buttonFrame.add(mEdit);
        buttonFrame.add(mSave);
        buttonFrame.add(mDelete);
        mSave.setEnabled(false);
    }


    /**
     * Enables and disables UI components
     * @param state: The state to put the components in
     */
    void setEnabled(Boolean state) {
        dataFrame.setEnabled(state);
        id.setEnabled(state);
        infoPanel.setEnabled(state);
        idFrame.setEnabled(state);
        id.setEnabled(state);
        buttonFrame.setEnabled(state);
        mAdd.setEnabled(state);
        mEdit.setEnabled(state);
        mDelete.setEnabled(state);
    }

    /**
     * Initializes the control buttons
     */
    void buttonControlInit() {
        dataFrame.add(buttonFrame);
    }

    /**
     * Sets the Editable state of the objects in the panel
     * @param state: The state to set the objects in
     */
    void setEditable(Boolean state) {

    }

    /**
     * Sets the tabbed pane
     * @param tabbedPane: The tabbed pane to set
     */
    public void setTab(JTabbedPane tabbedPane) {
        JTabbedPane tabbedPane1 = tabbedPane;
    }
    /**
     * Action handler
     */
    //@TODO implement info panel database collection functionality
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(actionEvent.getSource() == mEdit) {
            setEditable(true);
            mSave.setEnabled(true);
            addBool = false;
        } else if(actionEvent.getSource() == mAdd) {
            clearData();
            setEditable(true);
            mSave.setEnabled(true);
            addBool = true;
        } else if(actionEvent.getSource() == mDelete) {
            removeRecord();
        } else if(actionEvent.getSource() == mSave) {
            processSave();
            addBool = false;
            setEditable(false);
            mSave.setEnabled(false);
        }
    }

    /**
     * Clears all data in the information panel
     */
    void clearData() {
        id.setText("");
    }

    /**
     * Deletes a record from the database
     */
    void removeRecord() {

    }

    /**
     * Handles the mSave button's response
     */
    private void processSave() {
        if(addBool) {
            addRecord();
        } else {
            updateRecord();
        }
    }

    /**
     * Adds a record to the database
     */
    void addRecord() {

    }

    /**
     * Removes a record from the database
     */
    void updateRecord() {

    }
}

class personPanel extends infoPanel{
    private JLabel picture = new JLabel();
    private final JPanel pictureFrame = new JPanel();
    private final JPanel pictureContainer = new JPanel();
    private final JPanel filler = new JPanel();
    public final JTextArea firstName = new JTextArea();
    public final JTextArea lastName = new JTextArea();
    public final JTextArea dateOfBirth = new JTextArea();
    private final JPanel firstNameInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel lastNameInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel dateOfBirthInputFrame = new JPanel(new GridLayout(0, 1));

    personPanel(dbMgr db) {
        super(db);
    }

    /**
     * Builds upon the infoPanel core initialization function
     */
    void initialize() {
        super.initialize();
        // Initialize picture container
        pictureContainer.setLayout(new BoxLayout(pictureContainer, BoxLayout.PAGE_AXIS));

        TitledBorder pictureTitle = BorderFactory.createTitledBorder("Picture");
        pictureTitle.setTitleJustification(TitledBorder.CENTER);
        pictureFrame.setBorder(pictureTitle);

        picture = new JLabel();
        picture.setPreferredSize(new Dimension(250, 250));

        pictureContainer.add(pictureFrame);

        filler.setPreferredSize(new Dimension(250,250));
        pictureContainer.add(Box.createRigidArea(new Dimension(0, 5)));

        pictureContainer.add(filler);

        infoPanel.add(pictureContainer);

        firstName.setEditable(false);
        lastName.setEditable(false);
        dateOfBirth.setEditable(false);
        firstName.setMaximumSize(new Dimension(100, 15));   firstName.setLineWrap(true);
        lastName.setMaximumSize(new Dimension(100, 15));    lastName.setLineWrap(true);
        dateOfBirth.setMaximumSize(new Dimension(100, 15)); dateOfBirth.setLineWrap(true);

        // Containers for the data
        TitledBorder firstNameInputTitle = BorderFactory.createTitledBorder("First name");
        firstNameInputFrame.setBorder(firstNameInputTitle);
        firstNameInputFrame.add(firstName);

        TitledBorder lastNameInputTitle = BorderFactory.createTitledBorder("Last name");
        lastNameInputFrame.setBorder(lastNameInputTitle);
        lastNameInputFrame.add(lastName);

        TitledBorder dateOfBirthInputTitle = BorderFactory.createTitledBorder("Date of birth");
        dateOfBirthInputFrame.setBorder(dateOfBirthInputTitle);
        dateOfBirthInputFrame.add(dateOfBirth);

        dataFrame.add(firstNameInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(lastNameInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(dateOfBirthInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
    }

    /**
     * Enables and disables UI components
     * @param state: The state to put the components in
     */
    void setEnabled(Boolean state) {
        super.setEnabled(state);
        pictureFrame.setEnabled(state);
        picture.setEnabled(state);
        pictureContainer.setEnabled(state);
        firstName.setEnabled(state);
        lastName.setEnabled(state);
        dateOfBirth.setEnabled(state);
        firstNameInputFrame.setEnabled(state);
        lastNameInputFrame.setEnabled(state);
        dateOfBirthInputFrame.setEnabled(state);
    }

    void setEditable(Boolean state) {
        firstName.setEditable(state);
        lastName.setEditable(state);
        dateOfBirth.setEditable(state);
    }

    void clearData() {
        super.clearData();
        firstName.setText("");
        lastName.setText("");
        dateOfBirth.setText("");
    }

    protected void removeRecord() {

    }
}

class staffInfo extends personPanel {
    public final JTextArea phone = new JTextArea();
    public final JTextArea email = new JTextArea();
    public final JTextArea location = new JTextArea();
    public final JTextArea rank = new JTextArea();
    private final JPanel phoneInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel emailInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel locationInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel rankInputFrame = new JPanel(new GridLayout(0, 1));
    private staffBrowsePanel staffBPanel;

    public staffInfo(dbMgr db) {
        super(db);
    }

    /**
     * Builds upon the personPanel initialization function
     */
    public void initializeStaffInfo() {
        super.initialize();
        phone.setEditable(false);   email.setEditable(false);   location.setEditable(false); rank.setEditable(false);
        phone.setMaximumSize(new Dimension(105, 15));        phone.setLineWrap(true);
        email.setMaximumSize(new Dimension(100, 15));        email.setLineWrap(true);
        location.setMaximumSize(new Dimension(100, 15));     location.setLineWrap(true);
        rank.setMaximumSize(new Dimension(100, 15));         rank.setLineWrap(true);


        TitledBorder phoneInputTitle = BorderFactory.createTitledBorder("Phone number");
        phoneInputFrame.setBorder(phoneInputTitle);
        phoneInputFrame.add(phone);

        TitledBorder emailInputTitle = BorderFactory.createTitledBorder("Email address");
        emailInputFrame.setBorder(emailInputTitle);
        emailInputFrame.add(email);

        TitledBorder locationInputTitle = BorderFactory.createTitledBorder("Address");
        locationInputFrame.setBorder(locationInputTitle);
        locationInputFrame.add(location);

        TitledBorder rankInputTitle = BorderFactory.createTitledBorder("Rank");
        rankInputFrame.setBorder(rankInputTitle);
        rankInputFrame.add(rank);

        dataFrame.add(phoneInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(emailInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(locationInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(rankInputFrame);
        dataFrame.add(buttonFrame);
        buttonControlInit();

        infoPanel.add(dataFrame);
    }

    /**
     * Enables and disables UI components
     * @param state: The state to put the components in
     */
    public void setEnabled(Boolean state) {
        super.setEnabled(state);
        phone.setEnabled(state);
        email.setEnabled(state);
        location.setEnabled(state);
        rank.setEnabled(state);
        phoneInputFrame.setEnabled(state);
        emailInputFrame.setEnabled(state);
        locationInputFrame.setEnabled(state);
        rankInputFrame.setEnabled(state);
        infoPanel.setEnabled(state);
    }

    public void setEditable(Boolean state) {
        super.setEditable(state);
        phone.setEditable(state);
        email.setEditable(state);
        location.setEditable(state);
        rank.setEditable(state);
    }

    protected void clearData() {
        super.clearData();
        phone.setText("");
        email.setText("");
        location.setText("");
        rank.setText("");
    }

    protected void removeRecord() {
        db.removeRecord(Integer.parseInt(id.getText()), "staff");
        clearData();
    }

    protected void addRecord() {
        if(db.addStaffRecord(
                firstName.getText(), lastName.getText(), dateOfBirth.getText(), phone.getText(), email.getText(),
                location.getText(), rank.getText(), "NULL") != 0) {
            System.out.println("Could not add record!");
        }
    }

    protected void updateRecord() {
        if(db.updateStaffRecord(
                Integer.parseInt(id.getText()), firstName.getText(), lastName.getText(), dateOfBirth.getText(), phone.getText(),
                email.getText(), location.getText(), rank.getText(), "NULL") != 0) {
            System.out.println("Could not update record!");
        }

    }
}

class criminalInfo extends personPanel {
    public final JTextArea offense = new JTextArea();
    public final JTextArea sentenceLength = new JTextArea();
    public final JTextArea status = new JTextArea();
    private final JPanel offenseInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel sentenceLengthInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel statusInputFrame = new JPanel(new GridLayout(0, 1));

    public criminalInfo(dbMgr db) {
        super(db);
    }

    /**
     * Builds upon the personPanel initialization function
     */
    public void initialize() {
        super.initialize();

        offense.setEditable(false); sentenceLength.setEditable(false);  status.setEditable(false);
        offense.setMaximumSize(new Dimension(105, 15));         offense.setLineWrap(true);
        sentenceLength.setMaximumSize(new Dimension(105, 15));  sentenceLength.setLineWrap(true);
        status.setMaximumSize(new Dimension(105, 15));          status.setLineWrap(true);

        TitledBorder offenseInputTitle = BorderFactory.createTitledBorder("Offense");
        offenseInputFrame.setBorder(offenseInputTitle);
        offenseInputFrame.add(offense);

        TitledBorder sentenceLengthInputTitle = BorderFactory.createTitledBorder("sentenceLength");
        sentenceLengthInputFrame.setBorder(sentenceLengthInputTitle);
        sentenceLengthInputFrame.add(sentenceLength);

        TitledBorder statusInputTitle = BorderFactory.createTitledBorder("status");
        statusInputFrame.setBorder(statusInputTitle);
        statusInputFrame.add(status);

        dataFrame.add(offenseInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(sentenceLengthInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(statusInputFrame);
        buttonControlInit();

        infoPanel.add(dataFrame);
    }

    /**
     * Enables and disables UI components
     * @param state: The state to put the components in
     */
    public void setEnabled(Boolean state) {
        super.setEnabled(state);
        offense.setEnabled(state);
        sentenceLength.setEnabled(state);
        status.setEnabled(state);
        offenseInputFrame.setEnabled(state);
        sentenceLengthInputFrame.setEnabled(state);
        statusInputFrame.setEnabled(state);
        infoPanel.setEnabled(state);
    }

    public void setEditable(Boolean state) {
        super.setEditable(state);
        offense.setEditable(state);
        sentenceLength.setEditable(state);
        status.setEditable(state);
    }
    
    protected void clearData() {
        super.clearData();
        offense.setText("");
        sentenceLength.setText("");
        status.setText("");
    }

    protected void removeRecord() {
        db.removeRecord(Integer.parseInt(id.getText()), "criminals");
        clearData();
    }

    protected void addRecord() {
        db.addCriminalRecord(
                firstName.getText(), lastName.getText(), dateOfBirth.getText(), offense.getText(),
                sentenceLength.getText(), status.getText(), "NULL");
    }

    protected void updateRecord() {
        if(db.updateCriminalRecord(
                Integer.parseInt(id.getText()), firstName.getText(), lastName.getText(), dateOfBirth.getText(),
                offense.getText(), sentenceLength.getText(), status.getText(), "NULL") != 0) {
            System.out.println("Could not update record!");
        }
    }
}

class caseInfo extends infoPanel {
    public final JTextArea name = new JTextArea();
    public final JTextArea description = new JTextArea();
    public final JTextArea startDate = new JTextArea();
    public final JTextArea caseStarter = new JTextArea();
    public final JTextArea caseMaintainer = new JTextArea();
    private final JPanel nameInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel descriptionInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel startDateInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel caseStarterInputFrame = new JPanel(new GridLayout(0, 1));
    private final JPanel caseMaintainerInputFrame = new JPanel(new GridLayout(0, 1));

    public caseInfo(dbMgr db) {
        super(db);
    }

    /**
     * Function: initialize
     * Description: Builds upon the infoPanel initialization function
     */
    public void initialize() {
        super.initialize();

        name.setEditable(false);         description.setEditable(false);
        caseStarter.setEditable(false);  caseMaintainer.setEditable(false);  startDate.setEditable(false);
        name.setMaximumSize(new Dimension(100, 15));            name.setLineWrap(true);
        description.setMaximumSize(new Dimension(100, 15));     description.setLineWrap(true);
        startDate.setMaximumSize(new Dimension(100, 15));       startDate.setLineWrap(true);
        caseStarter.setMaximumSize(new Dimension(100, 15));     caseStarter.setLineWrap(true);
        caseMaintainer.setMaximumSize(new Dimension(100, 15));  caseMaintainer.setLineWrap(true);

        // Data containers
        TitledBorder nameInputTitle = BorderFactory.createTitledBorder("Case name");
        nameInputFrame.setBorder(nameInputTitle);
        nameInputFrame.add(name);

        TitledBorder descriptionInputTitle = BorderFactory.createTitledBorder("Case description");
        descriptionInputFrame.setBorder(descriptionInputTitle);
        descriptionInputFrame.add(description);

        TitledBorder startDateInputTitle = BorderFactory.createTitledBorder("Case start date");
        startDateInputFrame.setBorder(startDateInputTitle);
        startDateInputFrame.add(startDate);

        TitledBorder caseStarterInputTitle = BorderFactory.createTitledBorder("Case opener");
        caseStarterInputFrame.setBorder(caseStarterInputTitle);
        caseStarterInputFrame.add(caseStarter);

        TitledBorder caseMaintainerInputTitle = BorderFactory.createTitledBorder("Case maintainer");
        caseMaintainerInputFrame.setBorder(caseMaintainerInputTitle);
        caseMaintainerInputFrame.add(caseMaintainer);

        dataFrame.add(nameInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(descriptionInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(startDateInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(caseStarterInputFrame);
        dataFrame.add(Box.createRigidArea(new Dimension(1, 0)));
        dataFrame.add(caseMaintainerInputFrame);
        buttonControlInit();

        infoPanel.add(dataFrame);
    }

    /**
     * Function: setEnabled
     * Description: enables and disables UI components
     * @param state: The state to put the components in
     */
    public void setEnabled(Boolean state) {
        super.setEnabled(state);
        infoPanel.setEnabled(state);
        name.setEnabled(state);
        description.setEnabled(state);
        startDate.setEnabled(state);
        caseStarter.setEnabled(state);
        caseMaintainer.setEnabled(state);
        nameInputFrame.setEnabled(state);
        descriptionInputFrame.setEnabled(state);
        startDateInputFrame.setEnabled(state);
        caseStarterInputFrame.setEnabled(state);
        caseMaintainerInputFrame.setEnabled(state);
    }

    public void setEditable(Boolean state) {
        name.setEditable(state);
        description.setEditable(state);
        startDate.setEditable(state);
        caseStarter.setEditable(state);
        caseMaintainer.setEditable(state);
    }

    void clearData() {
        super.clearData();
        name.setText("");
        description.setText("");
        startDate.setText("");
        caseStarter.setText("");
        caseMaintainer.setText("");
    }

    protected void removeRecord() {
        db.removeRecord(Integer.parseInt(id.getText()), "cases");
        clearData();
    }

    protected void addRecord() {
        db.addCaseRecord(
                name.getText(), description.getText(), startDate.getText(), caseStarter.getText(),
                caseMaintainer.getText());
    }

    protected void updateRecord() {
        if(db.updateCaseRecord(
                Integer.parseInt(id.getText()), name.getText(), description.getText(), startDate.getText(),
                caseStarter.getText(), caseMaintainer.getText()) != 0) {
            System.out.println("Could not update record!");
        }
    }
}