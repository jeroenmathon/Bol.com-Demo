package bolDemo.tests.jeroenmathon;

import bolDemo.dataObjects.jeroenmathon.mCase;
import bolDemo.dataObjects.jeroenmathon.mCriminal;
import bolDemo.dataObjects.jeroenmathon.mStaff;
import bolDemo.database.jeroenmathon.dbMgr;

/**
 * Created by jeroen on 7-3-16.
 */
class dbTests {
    public static void main(String[] args) {
        dbMgr db = new dbMgr();

        mStaff staff = db.getStaff(1);
        System.out.println(
                "STAFF ID: " + staff.getId() + "\n" +
                "STAFF FIRST NAME: " + staff.getFirstName() + "\n" +
                "STAFF LAST NAME: " + staff.getLastName() + "\n" +
                "STAFF DOB: " + staff.getDateOfBirth() + "\n" +
                "STAFF PHONE: " + staff.getPhone() + "\n" +
                "STAFF EMAIL: " + staff.getEmail() + "\n" +
                "STAFF LOCATION: " + staff.getLocation() + "\n" +
                "STAFF RANK: " + staff.getRank() + "\n" +
                "STAFF PICTURE PATH: " + staff.getPicturePath() + "\n");

        mCriminal criminal = db.getCriminal(1);
        System.out.println(
                "CRIMINAL ID: " + criminal.getId() + "\n" +
                "CRIMINAL FIRST NAME: " + criminal.getFirstName() + "\n" +
                "CRIMINAL LAST NAME: " + criminal.getLastName() + "\n" +
                "CRIMINAL DOB: " + criminal.getDateOfBirth() + "\n" +
                "CRIMINAL OFFENSE: " + criminal.getOffense() + "\n" +
                "CRIMINAL SENTENCE LENGTH: " + criminal.getSentenceTime() + "\n" +
                "CRIMINAL STATUS: " + criminal.getStatus() + "\n" +
                "CRIMINAL PICTURE PATH: " + criminal.getPicturePath() + "\n");

        mCase mcase = db.getCase(1);
        System.out.println(
                "CASE ID: " + mcase.getId() + "\n" +
                        "CASE NAME: " + mcase.getName() + "\n" +
                        "CASE DESCRIPTION: " + mcase.getDescription() + "\n" +
                        "CASE START DATE: " + mcase.getStartDate() + "\n" +
                        "CASE STARTER: " + mcase.getCaseStarter() + "\n" +
                        "CASE MAINTAINER: " + mcase.getCaseMaintainer() + "\n");

    }
}
