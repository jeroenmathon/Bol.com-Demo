package bolDemo.database.jeroenmathon;

import bolDemo.dataObjects.jeroenmathon.mCase;
import bolDemo.dataObjects.jeroenmathon.mCriminal;
import bolDemo.dataObjects.jeroenmathon.mStaff;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by jeroen on 3-3-16.
 */

/**
 * dbMgr handles all communication between the application and the database
 */
public class dbMgr {
    private Connection conn = null;    // The sql connection
    private Statement stmt = null; // The sql statement we will use

    public dbMgr() {
        initializeTables();
    }

    /**
     * Establishes a connection to the MySQL Database
     * @return int: The return status of the function
     */
    private int connect() {
        try {
            // Load JDBC Drivers
            Class.forName("bolDemo.database.jeroenmathon.dbMgr");
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/bolDemo?" +
                    "user=bolDemo&password=bolDemo"
            );
            conn.setAutoCommit(false);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return -1;
        }

        return 0;
    }

    /**
     * Terminates a connection to the MySQL Database
     * @return int: The return status of the function
     */
    private int disconnect() {
        try {
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return -1;
        }

        return 0;
    }

    /**
     * Creates the needed tables in the database
     * @return int: The return status of the function
     */
    private int createTable(String sql) {
        // Connect to the database
        if (connect() != 0) {
            System.err.println("Database is not connected!");
            return -1;
        } else {
            try {
                // Create and execute the sql statement
                stmt = conn.createStatement();
                stmt.executeUpdate(sql);

                conn.commit();
                stmt.close();
                disconnect();
            } catch (Exception e) {
                disconnect();
                return -1;
            }
        }

        return 0;
    }

    /**
     * Executes a sql statement to update or insert data
     * @param sql: The sql statement to execute
     * @return int: The return status of the function
     */
    private int execModiData(String sql) {
        try {
            if (connect() == 0) {
                stmt = conn.createStatement();

                stmt.executeUpdate(sql);
                stmt.close();
                conn.commit();
                disconnect();
            } else {
                return -1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            disconnect();
            return -1;
        }

        return 0;
    }

    /**
     * Creates tables if they do not exist(Standard function of MySQL on table creation)
     * @return int: The return status of the function
     */
    private int initializeTables() {
        try {
            createTable("CREATE TABLE staff(id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,"
                    + "firstName TEXT, lastName TEXT, dateOfBirth TEXT, phone TEXT,"
                    + "email TEXT, location TEXT, rank TEXT, picturePath TEXT);");
            createTable("CREATE TABLE criminals(id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,"
                    + "firstName TEXT, lastName TEXT, dateOfBirth TEXT, offense TEXT,"
                    + "sentenceLength TEXT, status TEXT, picturePath TEXT);");
            createTable("CREATE TABLE cases(id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,"
                    + "name TEXT, description TEXT, startDate TEXT, caseStarter TEXT,"
                    + "caseMaintainer TEXT);");

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

        return 0;
    }

    /**
     * Adds a mStaff record to the mStaff table
     * @param firstName: The first name of the mStaff member
     * @param lastName: The last name of the mStaff member
     * @param dateOfBirth: The date of birth of the mStaff member
     * @param phone: The phone number of the mStaff member
     * @param email: The email address of the mStaff member
     * @param location: The location of the mStaff member
     * @param rank: The rank of the mStaff member
     * @param picturePath: The path to the picture of the staff member
     * @return int: The return status of the function
     */
    public int addStaffRecord(
            String firstName, String lastName,
            String dateOfBirth, String phone,
            String email, String location,
            String rank, String picturePath) {

        return execModiData(
                "INSERT INTO staff(firstName, lastName, dateOfBirth, phone, email, location, rank, picturePath)"
                        + " VALUES (" + "\"" + firstName + "\"" + ", " + "\"" + lastName + "\"" + ", " + "\"" + dateOfBirth + "\""
                        + ", " + "\"" + phone + "\"" + ", " + "\"" + email + "\"" + ", " + "\"" + location + "\"" + ", " + "\""
                        + rank + "\"" + ", " + "\"" + picturePath + "\"" + ");");

    }

    /**
     * Adds a mCriminal record to the mStaff table
     * @param firstName: The first name of the mCriminal
     * @param lastName: The last name of the mCriminal
     * @param dateOfBirth: The date of birth of the mCriminal
     * @param offense: The offense the mCriminal committed
     * @param sentenceLength: The length of the criminals sentence
     * @param status: The current status of the mCriminal
     * @param picturePath: The path to the picture of the criminal
     * @return int: The return status of the function
     */
    public int addCriminalRecord(
            String firstName, String lastName,
            String dateOfBirth, String offense,
            String sentenceLength, String status,
            String picturePath) {
        return execModiData(
                "INSERT INTO criminals (firstName, lastName, dateOfBirth, offense, sentenceLength, status, picturePath)"
                        + "VALUES (" + "\"" + firstName + "\"" + ", " + "\"" + lastName + "\"" + ", " + "\"" + dateOfBirth
                        + "\"" + ", " + "\"" + offense + "\"" + ", " + "\"" + sentenceLength + "\"" + ", " + "\""
                        + status + "\"" + ", " + "\"" + picturePath + "\"" + ");");
    }

    /**
     * Adds a case record to the mStaff table
     * @param name: The name of the case
     * @param description: The description of the case
     * @param startDate: The date the case was opened
     * @param caseStarter: The mStaff member that opened the case
     * @param caseMaintainer: The maintainer of the case
     * @return int: The return status of the function
     */
    public int addCaseRecord(
            String name, String description,
            String startDate, String caseStarter,
            String caseMaintainer) {

        return execModiData(
                "INSERT INTO cases (name, description, startDate, caseStarter, caseMaintainer)"
                        + "VALUES (" + "\"" + name + "\"" + ", " + "\"" + description + "\"" + ", "
                        + "\"" + startDate + "\"" + ", " + "\"" + caseStarter + "\"" + ", "
                        + "\"" + caseMaintainer + "\"" + ");"
        );
    }

    /**
     * Updates an existing staff record with new information
     * @param id: The id of the record to update
     * @param firstName: The first name of the staff member
     * @param lastName: The last name of the staff member
     * @param dateOfBirth: The date of birth of the staff member
     * @param phone: The phone number of the staff member
     * @param email: The email address of the staff member
     * @param location: The location of the staff member
     * @param rank: The rank of the staff member
     * @param picturePath: The path to the picture of the staff member
     * @return int: The status if the function executed correctly
     */
    public int updateStaffRecord(
            int id, String firstName, String lastName, String dateOfBirth, String phone,
            String email, String location, String rank, String picturePath) {

        if(connect() == 0) {
            return execModiData(
                    "UPDATE staff SET " +
                    "firstName = " + "\"" + firstName + "\"" + ", lastName = " +"\"" + lastName + "\"" +
                    ", dateOfBirth = " + "\"" + dateOfBirth + "\"" + ", phone = " + "\"" + phone + "\"" +
                    ", email = " + "\"" + email + "\"" + ", location =  " + "\"" + location + "\"" +
                    ", rank = " + "\"" + rank + "\"" + ", picturePath = NULL WHERE id=" + id + ";");
        } else {
            return -1;
        }
    }

    /**
     * Updates an existing criminal record with new information
     * @param id: The id of the record to update
     * @param firstName: The first name of the criminal
     * @param lastName: The last name of the criminal
     * @param dateOfBirth: The date of birth of the criminal
     * @param offense: The offense of the criminal
     * @param sentenceLength: The sentence length
     * @param status: The status of the criminal
     * @param picturePath: The path to the picture of the criminal
     * @return int: The status if the function executed correctly
     */
    public int updateCriminalRecord(
            int id, String firstName, String lastName, String dateOfBirth, String offense,
            String sentenceLength, String status, String picturePath) {

        if(connect() == 0) {
            return execModiData(
                    "UPDATE criminals SET " +
                    "firstName = " + "\"" + firstName + "\"" + ", lastName = " + "\"" + lastName + "\"" +
                    ", dateOfBirth = " + "\"" + dateOfBirth + "\"" + ", offense = " + "\"" + offense + "\"" +
                    ", sentenceLength = " + "\"" + sentenceLength + "\"" + ", status = " + "\"" + status + "\"" +
                    ", picturePath = \"NULL\" WHERE id=" + id + ";");
        } else {
            return -1;
        }
    }

    /**
     * Updates an existing case record with new information
     * @param id: The id of the record to update
     * @param name: The name of the case
     * @param description: The description of the case
     * @param startDate: The date the case has been opened
     * @param caseStarter: The staff member that opened the case
     * @param caseMaintainer: The staff member that maintains the case
     * @return int: The status if the function executed correctly
     */
    public int updateCaseRecord(
            int id, String name, String description, String startDate, String caseStarter,
            String caseMaintainer) {

        if(connect() == 0) {
            return execModiData(
                    "UPDATE cases SET " +
                    "name = " + "\"" + name + "\"" + ", description = " + "\"" + description + "\"" +
                    ", startDate = " + "\"" + startDate + "\"" + ", caseStarter = " + "\"" + caseStarter + "\"" +
                    ", caseMaintainer = " + "\"" + caseMaintainer + "\"" + " WHERE id=" + id + ";");
        } else {
            return -1;
        }
    }

    //@TODO Move get object functions into a object to reduce code

    /**
     * Returns a staff object filled with information of the record corresponding with the id
     * @param id: The id to obtain the record from
     * @return mStaff: The staff object filled with retrieved information
     */
    public mStaff getStaff(int id) {
        if (connect() == 0) {
            try {
                stmt = conn.createStatement();

                String sql = "SELECT * FROM staff where id=" + id;
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    if (id == rs.getInt("id")) {
                        return new mStaff(
                                rs.getInt("id"), rs.getString("firstName"), rs.getString("lastName"),
                                rs.getString("dateOfBirth"), rs.getString("picturePath"), rs.getString("phone"),
                                rs.getString("email"), rs.getString("location"), rs.getString("rank"));
                    }
                }
                disconnect();
            } catch (SQLException e) {
                e.printStackTrace();
                disconnect();
            }
        }

        return null;
    }

    /**
     * Returns a criminal object filled with information of the record corresponding with the id
     * @param id: The id to obtain the record from
     * @return mStaff: The criminal object filled with retrieved information
     */
    public mCriminal getCriminal(int id) {
        if (connect() == 0) {
            try {
                stmt = conn.createStatement();

                String sql = "SELECT * FROM criminals where id=" + id;
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    if (id == rs.getInt("id")) {
                        return new mCriminal(
                                rs.getInt("id"), rs.getString("firstName"), rs.getString("lastName"),
                                rs.getString("dateOfBirth"), rs.getString("picturePath"), rs.getString("sentenceLength"),
                                rs.getString("offense"), rs.getString("status"));
                    }
                }
                disconnect();
            } catch (SQLException e) {
                e.printStackTrace();
                disconnect();
            }
        }

        return null;
    }

    /**
     * Returns a case object filled with information of the record corresponding with the id
     * @param id: The id to obtain the record from
     * @return mStaff: The case object filled with retrieved information
     */
    public mCase getCase(int id) {
        if (connect() == 0) {
            try {
                stmt = conn.createStatement();

                String sql = "SELECT * FROM cases where id=" + id;
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    if (id == rs.getInt("id")) {
                        return new mCase(
                                rs.getInt("id"), rs.getString("name"), rs.getString("description"),
                                rs.getString("startDate"), rs.getString("caseStarter"), rs.getString("caseMaintainer"));
                    }
                }
                disconnect();
            } catch (SQLException e) {
                e.printStackTrace();
                disconnect();
            }
        }

        return null;
    }

    /**
     * Returns a list with all the id's in a table
     * @param table: The table to obtain the id's from
     * @return ArrayList<Integer>: A list filled with the id's from the table
     */
    public ArrayList<Integer> getIds(String table) {
        if (connect() == 0) {
            try {
                ArrayList<Integer> ids = new ArrayList<>();
                stmt = conn.createStatement();

                String sql = "SELECT id FROM " + table;
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ids.add(rs.getInt("id"));
                }
                disconnect();
                return ids;
            } catch (SQLException e) {
                e.printStackTrace();
                disconnect();
            }
        }

        return null;
    }

    /**
     * Removes a record from a table
     * @param id : The id of the record
     * @param table : The tables to remove a record from
     * @return int: The return status of the function
     */
    public int removeRecord(int id, String table) {
        try {
            if (connect() == 0) {
                stmt = conn.createStatement();

                String sql = "DELETE FROM " + table + " where id=" + id;
                stmt.executeUpdate(sql);
                stmt.close();
                conn.commit();
                disconnect();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            disconnect();

            return -1;
        }

        return 0;
    }

    /**
     * Returns the amount of rows in a table
     * @param table: The table to get the rows from
     * @return Int: The amount of rows in a table
     */
    public int rowCount(String table) {
        try {
            if (connect() == 0) {
                stmt = conn.createStatement();
                String sql = "SELECT COUNT(*) as rowcount FROM " + table + ";";
                ResultSet rs = stmt.executeQuery(sql);
                int count;

                rs.next();
                count = rs.getInt("rowcount");
                rs.close();
                disconnect();

                return count;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            disconnect();
            return -1;
        }

        return 0;
    }
}
